# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from sql import Table
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.modules.product import price_digits

STATES = {
    'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
}


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'
    sale_price_w_tax = fields.Numeric('Sale Price Tax',
        digits=price_digits, states=STATES)
    final_cost_w_tax = fields.Function(fields.Numeric('Landed Cost Tax',
        digits=price_digits), 'get_final_cost_w_tax')
    description = fields.Text('Description', states=STATES)
    old_cost_price = fields.Function(fields.Numeric('Old Cost Price', digits=price_digits,
        states={'readonly': True}), 'get_last_cost')
    old_sale_price = fields.Numeric('Old Sale Price', digits=price_digits,
        states={'readonly': True})
    profit = fields.Function(fields.Float('Profit', digits=(16, 2),
        depends=['sale_price_w_tax']),'on_change_with_profit')
    discount = fields.Float("Discount")

    @fields.depends('sale_price_w_tax', 'final_cost_w_tax')
    def on_change_with_profit(self, name=None):
        profit = 0
        if self.sale_price_w_tax and self.final_cost_w_tax and self.final_cost_w_tax > 0:
            profit = float((self.sale_price_w_tax / self.final_cost_w_tax) * 100)
        return profit

    @staticmethod
    def default_discount():
        return 0.0

    @classmethod
    def search_rec_name(cls, name, clause):
        target = clause[2:][0][1:][:-1]
        #Get products by code
        target_words = target.split(' ')
        dom_search = []
        for tw in target_words:
            # Ignore words with less 3 letter because otherwise
            # can be huge search
            if len(tw) <= 1:
                continue
            clause = ['OR',
                ('product.template.name', 'ilike', '%' + tw + '%'),
                ('product.code', 'ilike', '%' + tw + '%'),
                ('product.description', 'like', '%' + tw + '%'),
                ('origin.sale.party.name', 'ilike', '%' + tw + '%', 'sale.line'),
            ]
            dom_search.append(clause)

        return dom_search

    @classmethod
    def create(cls, vlist):
        moves = super(Move, cls).create(vlist)

        for move in moves:
            sale_price_w_tax = getattr(move.product.template, 'sale_price_w_tax', None)
            if sale_price_w_tax:
                move.old_sale_price = move.product.template.sale_price_w_tax
            move.old_cost_price = move.product.last_cost
            move.description = move.product.description
            move.save()
        return moves

    def get_last_cost(self, name=None):
        res = 0
        if self.product:
            PurchaseLine = Pool().get('purchase.line')
            products = PurchaseLine.search_read([
                ('product', '=', self.product.id),
                ('purchase.purchase_date', '<', self.effective_date),
            ], fields_names=['unit_price'], order=[('create_date', 'DESC')], limit=1)
            if products:
                res = products[0]['unit_price']
        return res

    def get_final_cost_w_tax(self, name=None):
        Tax = Pool().get('account.tax')
        res = self.unit_price
        supplier_taxes = []

        if self.product.supplier_taxes_used and self.unit_price:
            for tax in self.product.supplier_taxes_used:
                if tax.type == 'percentage' and tax.rate > 0:
                    supplier_taxes.append(tax)
            tax_list = Tax.compute(supplier_taxes,
                self.unit_price or Decimal('0.0'), 1)
            res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
            res = res + self.unit_price
            res = res.quantize(
                Decimal(1) / 10 ** self.__class__.final_cost_w_tax.digits[1]
            )
        return res

    @fields.depends('unit_price', 'discount')
    def on_change_discount(self):
        if self.discount and self.unit_price:
            self.unit_price = self.set_new_unit_price(self.discount)

    def set_new_unit_price(self, discount):
        value = discount / 100
        new_unit_price = float(self.unit_price) * (1 + value)
        return Decimal(new_unit_price).quantize(Decimal(100) ** -2)


class UpdateUnitPriceStart(ModelView):
    'Update Unit Price Wizard Start'
    __name__ = 'stock_product_cost.update_unit_price.start'
    discount = fields.Float('Discount', digits=(2,2), required=True)


class UpdateUnitPrice(Wizard):
    'Update Unit Price Wizard'
    __name__ = 'stock_product_cost.update_unit_price'
    start = StateView('stock_product_cost.update_unit_price.start',
        'stock_product_cost.stock_update_unit_price_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        ShipmentIn = Pool().get('stock.shipment.in')
        Move = Pool().get('stock.move')
        shipments = ShipmentIn.browse(Transaction().context['active_ids'])
        for shipment in shipments:
            if shipment.state != 'draft':
                continue
            for move in shipment.incoming_moves:
                unit_price = move.set_new_unit_price(self.start.discount)
                Move.write([move], {
                    'discount': self.start.discount,
                    'unit_price': unit_price,
                })

        return 'end'
