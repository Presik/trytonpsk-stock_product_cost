# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def proceed(cls, purchases):
        super(Purchase, cls).proceed(purchases)
        cls.update_last_cost(purchases)

    @classmethod
    def update_last_cost(cls, purchases):
        for purchase in purchases:
            for l in purchase.lines:
                l.product.last_cost = l.unit_price
                l.product.save()
