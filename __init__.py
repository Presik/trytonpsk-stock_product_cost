# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import move
from . import product
from . import purchase


def register():
    Pool.register(
        product.Template,
        product.Product,
        purchase.Purchase,
        move.Move,
        move.UpdateUnitPriceStart,
        module='stock_product_cost', type_='model')
    Pool.register(
        move.UpdateUnitPrice,
        module='stock_product_cost', type_='wizard')
